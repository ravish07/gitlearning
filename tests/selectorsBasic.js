import { Selector } from 'testcafe';
import { assertTextIsCorrect } from './commons';

fixture `selectorsBasic`
.page `https://demoqa.com/text-box`;

test
.skip
('selectorsFirstTest', async t => {
    console.log('inside selectors first test');
    const webtablesLink = Selector('#item-3');
    await t
    .click(webtablesLink)
    .wait(5000);
})

test
.skip
('SelectorSecondTest', async t => {
    console.log('inside selectors second test');
    const webtablesLink = Selector('#item-3');
    await t
    .click(webtablesLink)
    .wait(5000);

    const webtablesHeader = Selector('.main-header');
    const actualHeader = await webtablesHeader.innerText;
    const expectedHeader = 'Web Tables';

    if(actualHeader === expectedHeader){
        console.log("result as expected");
        console.log(actualHeader);
    }else{
        console.log("result failed");
        console.log(actualHeader);
    }
})

test
.skip
('childNode', async t => {
    const webtableElementLink = Selector('ul').child(3);
    await t.click(webtableElementLink);
    console.log("inside child node test");
})

test('assertOperation', async t => {
  const headerElement = Selector('.main-header');
  await assertTextIsCorrect(
      t,
      headerElement,
      'Text Box',
      'Header is matching'
  );  

  console.log('after failure');
})