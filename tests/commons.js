export const assertTextIsCorrect = async (
    t,
    testElement,
    expectedText,
    message
) => {
    const text = await testElement.withExactText(expectedText).exists;
    console.log('assertTextIsCorrect inside:', text);
    await t.expect(text).ok(message);
}